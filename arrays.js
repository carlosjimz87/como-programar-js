/**  
 * MAYOR VELOCIDAD EN:
 * - aañadir elemento
 * - eliminar elimento
 * - modificar elementos
 * 
 * PERMITE 
 * - ordenar elementos
*/

// Como crear una array
var lista = [null , 2 , "Hola", 4.56, false]

// imprimiendo el array
console.log(lista)

// imprimiendo el tipo de dato de la array (en JS todos son object)
console.log(typeof(lista))

// extrayendo un elemento de la array
console.log(lista[2])

// iterando los elementos de la array para imprimirlos individualmente
for (let i = 0; i < lista.length; i++) {
        console.log(lista[i])
    }                                                                       

// modificando la lista
lista[2] = "Mundo"
lista[4] = true
console.log(lista)

// anulando un elemnto de la lista
lista[4] = null
console.log(lista)

// añadir un elemento de la lista al final
lista.push("Santi")
console.log(lista)

// eliminar un elemento de la lista (por valor)
lista.pop("Santi")
console.log(lista)

// error de indexado al acceder a la lista --> ERROR (list index out of range)
console.log(lista[200])

console.log(lista[2])
// CICLOS DEFINIDOS

// var i = 0;
// i = i+1;    // +1
// i+=1;       // +1
// i++;        // +1    (SOLO JAVASCRIPT)


// un ciclo que imprima 10 veces hola con las iteraciones de uno en uno
for(i=0 ; i<10 ; i++){
    console.log("hola",i)
}

// console.log("fin")

// un ciclo que imprima 5 veces hola con las iteraciones de dos en dos
// for(var i=0 ; i<10 ; i++){
//     console.log("hola",i)
// }

// CICLOS INDEFINIDOS
// var i = 0;
// while(i<0){
//     console.log("hola",i)
//     i++;
// }

var i = 0;
do{
    console.log("hola",i)
    i++;
}while(i<0)

/**
 * 
 * 1 Write a function that create a single string, from two given strings (separated by a space).
 * 2 Write a function that find the area of a circle, given its radius.
 * 3 Write a function that returns True if a number is divisible by 3, otherwise returns False.
 * 4 Write a program to find those numbers which are divisible by 3 and multiple of 2, and also are between 10 and 300 (both included).
 * 
 * 
 */

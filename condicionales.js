var a = 0;
var b = 1;

/*********  CONDICIONALES *********/

// operador AND

// a   b   RESULTADO ( 0 = false, 1 = true)
// 0   0   0
// 0   1   0
// 1   0   0
// 1   1   1

//   1 .if else   //

// if(a && b){ // = 1
//     console.log("Todos son unos"); 
    
// }
// else{   // = 0

//     console.log("Al menos hay un cero");
// }


//   2 .if else anidado   //

// if(a && b){ // = 1
//     console.log("Todos son unos"); 
    
// }
// else{   // = 0

//     console.log("Al menos hay un cero");

//     if(a == 0 && b == 0){ // ambas variables son 0
//         console.log("Ambos son cero");

//     }

// }


//   3 .if else if else  //


// if(a && b){ // TODO UNO
//     console.log("Todos son unos"); 
    
// }
// else if(a == 0 && b == 0){ // TODO CERO
//     console.log("Ambos son cero");
    
// }
// else{   // AL MENOS UN CERO

//     console.log("Al menos hay un cero");
// }


//   4. switch - case

var numero = 1  // puede tomar los valores 1,2,3,4

switch (numero){

    case 1:
    console.log("Es uno")
    break;

    case 2:
    console.log("Es dos")
    break;

    case 3:
    console.log("Es tres")
    break;

    default:
    console.log("Es cuatro")
    break;

}

